<?php
	session_set_cookie_params(time() + 3600 * 24 * 100);// 100 days
	session_start();
	if(isset($_GET["logout"])){
		$_SESSION["loggedin"] = false;
	}
	if($_SESSION["loggedin"] != true){
		header('LOCATION:login.php');
	}
?>
 <!DOCTYPE html>
<html lang="no" ng-app="picsApp">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.js"></script>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-more.js"></script>
<script src="http://code.highcharts.com/modules/solid-gauge.js"></script>
<script src="pics.js"></script>
<head>
	<title>Raspberry Pi Control Station</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
    <link rel="stylesheet" href="style.css" />
</head>
<body ng-controller="ListController as list">

	<div class="selected" ng-show="list.selected">
		<a href="javascript:void(0);" class="lr-button" ng-click="list.selected=null" ng-hide="list.hideLists"><i class="fa fa-close"></i></a>
		<h2>
			<a style="text-decoration: none;" href="index.html?single={{list.selected.hostname}}" title="Show only this Pi">
				{{list.selected.hostname}} 
			</a>
			<i class="fa fa-circle {{list.selected.online ? 'color-green' : 'color-gray'}}"></i>
		</h2>

		<table class="info">
			<tbody>
				<tr>
					<td>
						<i class="fa fa-fire fa-lg"></i> {{list.selected.temp}} &deg;C
					</td>
					<td title="{{list.selected['last user']}}"><i class="fa fa-user fa-lg"></i> {{list.selected.users}}</td>
					<td title="{{list.selected.top}}"><i class="fa fa-gears  fa-lg"></i> {{list.selected.processes}}</td>
				</tr>
				<tr>
					<td><i class="fa fa-calendar-check-o fa-lg"></i> {{list.selected.time}}</td>
					<td><i class="fa fa-clock-o fa-lg"></i> {{list.selected.uptime}}</td>
					<td>
						<i class="fa fa-sitemap fa-lg"></i>
						<span ng-repeat="ip in list.selected.ips">
			  				<a href="http://{{ip}}" target="_blank">{{ip}}</a><br />
			  			</span>
					</td>
				</tr>
			</tbody>
		</table>

		<div style="height: 200px;">
			<div id="chartDiskSpace" class="chart"></div>
			<div id="chartMemory" class="chart"></div>
			<div id="chartCPU" class="chart"></div>
		</div>
		
		<a href="javascript:void(0);" class="lr-command" ng-show="list.showPowerButtons">Shutdown<br /></a>
		<a href="javascript:void(0);" class="lr-command" ng-show="list.showPowerButtons">Reboot<br /></a>
		<a href="javascript:void(0);" class="lr-command" ng-show="list.showUpgradeButtons" title="Current script version: {{item.scriptversion}}">Upgrade script<br /></a>
		<a href="javascript:void(0);" class="lr-command" ng-show="list.showUpgradeButtons">Upgrade<br /></a>

		<a href="javascript:void(0);" title="Shutdown options" class="lr-button {{list.showPowerButtons ? '' : 'color-gray'}}" ng-click="list.showUpgradeButtons = false; list.showPowerButtons = !list.showPowerButtons"><i class="fa fa-power-off"></i></a>
		<a href="javascript:void(0);" title="Upgrade options" class="lr-button {{list.showUpgradeButtons ? '' : 'color-gray'}}" ng-click="list.showPowerButtons = false; list.showUpgradeButtons = !list.showUpgradeButtons"><i class="fa fa-arrow-circle-up"></i></a>
		<a href="javascript:void(0);" title="Show raw data" class="lr-button {{list.showRaw ? '' : 'color-gray'}}" ng-click="list.showRaw = !list.showRaw"><i class="fa fa-file-text-o"></i></a>

		<table ng-show="list.showRaw" class="data">
			<thead>
				<tr>
					<th>Parameter</th>
					<th>Attribute</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="item in list.selected.attributes">
					<td>{{item}}</td>
					<td style="white-space: pre;">{{list.selected[item]}}</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="lists" ng-hide="list.hideLists">
		<h2>Online</h2>
	  	<table class="data">
	  		<thead>
		  		<tr>
			  		<th>Host name</th>
			  		<th>IP(s)</th>
			  		<th>Temperature</th>
			  		<th>Uptime</th>
			  		<th>Users</th>
			  	</tr>
		  	</thead>
		  	<tbody>
			  	<tr ng-repeat="item in list.onlinePis" ng-click="list.select(item)">
			  		<td ng-hide="list.isSelected(item)">{{item.hostname}}</td>
			  		<td ng-show="list.isSelected(item)"><strong><i class="fa fa-caret-right" /> {{item.hostname}}</strong></td>
			  		<td>
			  			<span ng-repeat="ip in item.ips">
			  				<a href="http://{{ip}}" target="_blank">{{ip}}</a><br />
			  			</span>
		  			</td>
			  		<td>{{item.temp}}&deg;C</td>
			  		<td>{{item.uptime}}</td>
			  		<td title="{{item['last user']}}">{{item.users}}</td>
			  	</tr>
			</tbody>
	  	</table>

		<h2>Offline</h2>
		<table class="data">
			<thead>
				<tr>
					<th>Host name</th>
					<th>IP(s)</th>
					<th>Uptime</th>
					<th>Script version</th>
					<th>Last seen</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="item in list.offlinePis" ng-click="list.select(item)">
			  		<td ng-hide="list.isSelected(item)">{{item.hostname}}</td>
			  		<td ng-show="list.isSelected(item)"><strong><i class="fa fa-caret-right" /> {{item.hostname}}</strong></td>
					<td>
			  			<span ng-repeat="ip in item.ips">
			  				<a href="http://{{ip}}" target="_blank">{{ip}}</a><br />
			  			</span>
		  			</td>
					<td>{{item.uptime}}</td>
					<td>{{item.scriptversion}}</td>
					<td>{{item.time}}</td>
				</tr>
			</tbody>
		</table>
	</div>

	<a class="logout" href="?logout=true">Log out</a>

</body>
</html> 