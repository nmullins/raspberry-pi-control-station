<?php
	session_set_cookie_params(time() + 3600 * 24 * 100);// 100 days
	session_start();

	$params = json_decode(file_get_contents('php://input'),true);
	if(isset($params) && isset($params["un"]) || isset($params["pw"])){
		if($params["un"]=="erlend" && $params["pw"] == file_get_contents("data/pw.txt")){
			$_SESSION["loggedin"] = true;
			die("ok");
		}
		die("error");
	}

	if($_SESSION["loggedin"] == true){
		header('LOCATION:index.php');
	}
?>
<!DOCTYPE html>
<html lang="no" ng-app="picsApp">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.js"></script>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-more.js"></script>
<script src="http://code.highcharts.com/modules/solid-gauge.js"></script>
<script src="pics.js"></script>
<head>
	<title>Raspberry Pi Control Station</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
    <link rel="stylesheet" href="style.css" />
</head>
<body>
	<div style="margin-top: 200px;" ng-controller="LoginController as login">
		<div ng-show="message" class="alert alert-danger">{{message}}</div>
		<form name="form" ng-submit="login.login()" role="form">
		    <div class="form-group">
		        <label for="username">Username</label>
		        <i class="fa fa-key"></i>
		        <input type="text" name="username" id="username" class="form-control" ng-model="username" required />
		        <span ng-show="form.username.$dirty && form.username.$error.required" class="help-block">Username is required</span>
		    </div>
		    <div class="form-group">
		        <label for="password">Password</label>
		        <i class="fa fa-lock"></i>
		        <input type="password" name="password" id="password" class="form-control" ng-model="password" required />
		        <span ng-show="form.password.$dirty && form.password.$error.required" class="help-block">Password is required</span>
		    </div>
		    <div class="form-actions">
		        <button type="submit" ng-disabled="form.$invalid || dataLoading" class="btn btn-danger">Login</button>
		        <i ng-if="dataLoading" class="fa fa-spinner"></i>
		    </div>
		</form>
	</div>
</body>
</html> 