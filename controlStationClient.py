#!/usr/bin/python
import socket
import fcntl
import struct
import subprocess
import sys
import os
import wget
import time
import datetime
import json
from socket import error as socket_error

version = 13
port = 15905
updateUrl = "http://www.erlenddahl.net/controlStationClient.py"

print "Starting control station, version " + str(version)
    
def tcp_server_wait ( numofclientwait, port ):
    global s2
    s2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
    s2.bind(('',port)) 
    s2.listen(numofclientwait) 

def tcp_finish():
    try:
        s.shutdown(socket.SHUT_RDWR)
    except Exception as e:
        print e
        pass
    try:
        s2.shutdown(socket.SHUT_RDWR)
    except Exception as e:
        print e
        pass
    try:
        s.close()
    except Exception as e:
        print e
        pass
    try:
        s2.close()
    except Exception as e:
        print e
        pass

def tcp_server_next ( ):
    global s
    s = s2.accept()[0]
   
def tcp_write(D):
    print "SENDING: " + D
    s.send(D)
   
def tcp_flush():
    s.send('\r')
    
def tcp_read( ):
    b = ''
    while True:
        a = s.recv(1)
        if a == "\r":
            break
        b = b + a
    return b

def tcp_close( ):
    s.close()
    return 

def get_ram():
    "Returns a tuple (total ram, available ram) in megabytes. See www.linuxatemyram.com"
    try:
        s = subprocess.check_output(["free","-m"])
        lines = s.split('\n')       
        return int(lines[1].split()[1]), int(lines[2].split()[3])
    except:
        return 0, 0

def get_process_count():
    "Returns the number of processes"
    try:
        s = subprocess.check_output(["ps","-e"])
        return len(s.split('\n'))       
    except:
        return 0

def get_up_stats():
    "Returns a tuple (uptime, 1 min load average, 5 min load average, 15 min load average)"
    s = ""
    try:
        s = subprocess.check_output(["uptime"])
        users = int(s.split(" user")[0].split(",")[-1].strip())
        load_split = s.split('load average: ')
        load_one = float(load_split[1].split(',')[0])
        load_five = float(load_split[1].split(',')[1])
        load_fifteen = float(load_split[1].split(',')[2])
        up = load_split[0]
        up_pos = up.rfind(',',0,len(up)-4)
        up = up[:up_pos].split('up ')[1]
        return up, load_one, load_five, load_fifteen, users    
    except Exception as e:
        print s
        print e
        return '' , 0, 0, 0, 0

def get_connections():
    "Returns the number of network connections"
    try:
        s = subprocess.check_output(["netstat","-tun"])
        return len([x for x in s.split() if x == 'ESTABLISHED'])
    except:
        return 0

def get_temperature():
    "Returns the temperature in degrees C"
    try:
        s = subprocess.check_output(["/opt/vc/bin/vcgencmd","measure_temp"])
        return float(s.split('=')[1][:-3])
    except:
        return 0

def get_cpu_speed():
    "Returns the current CPU speed"
    f = os.popen('/opt/vc/bin/vcgencmd get_config arm_freq')
    cpu = f.read().rstrip()
    return cpu

def get_ip(ifname):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            struct.pack('256s', ifname[:15])
        )[20:24])
    except:
        return "0.0.0.0"

def disk_usage(path):
    """Return disk usage statistics about the given path.

    Returned value is a tuple with the amount of total, used and free space, in bytes."""

    st = os.statvfs(path)
    free = st.f_bavail * st.f_frsize
    total = st.f_blocks * st.f_frsize
    used = (st.f_blocks - st.f_bfree) * st.f_frsize
    return total, used, free

def get_serial():
    # Extract serial from cpuinfo file
    cpuserial = "0000000000000000"
    try:
        f = open('/proc/cpuinfo','r')
        for line in f:
            if line[0:6]=='Serial':
                cpuserial = line[10:26]
        f.close()
    except:
        cpuserial = ""
    return cpuserial

def get_last():
    s = subprocess.check_output(["last", "-n", "3"])
    return s

def get_top():
    s = subprocess.check_output(["top", "-b", "-n", "1"]).split("\n")
    return "\n".join(s[6:12])

count = 0
while True:
    try:
        tcp_server_wait ( 3, port )
        break
    except Exception as e:
        count = count + 1
        print "Failed to start TCP server, " + str(count)
        print e
        time.sleep(5)
        pass

print "TCP server listening."

while True:
    try:
        tcp_server_next()
        params = tcp_read().split(';')
        print "Got request: ", params
        cmd = params[0]
        
        if cmd == "info":
            serial = get_serial()
            if serial == "": serial = socket.gethostname()
            ramt, rama = get_ram()
            uptimestring, cpu_one, cpu_five, cpu_fifteen, users = get_up_stats()
            diskt, disku, diskf = disk_usage('/')

            jsonData = {}

            jsonData['hostname'] = socket.gethostname()
            jsonData['local timestamp'] = int(round(time.time() * 1000))
            jsonData['scriptversion'] = version
            jsonData['serial'] = serial
            jsonData['ip eth0'] = get_ip('eth0')
            jsonData['ip wlan0'] = get_ip('wlan0')
            jsonData['ram total'] = ramt
            jsonData['ram available'] = rama
            jsonData['processes'] = get_process_count()
            jsonData['connections'] = get_connections()
            jsonData['temp'] = get_temperature()
            jsonData['cpu'] = get_cpu_speed()
            jsonData['uptime'] = uptimestring
            jsonData['cpu 1min lavg'] = cpu_one
            jsonData['cpu 5min lavg'] = cpu_five
            jsonData['cpu 15min lavg'] = cpu_fifteen
            jsonData['users'] = users
            jsonData['last user'] = get_last()
            jsonData['top'] = get_top()
            jsonData['disk total'] = diskt / 1024 / 1024
            jsonData['disk used'] = disku / 1024 / 1024
            jsonData['disk free'] = diskf / 1024 / 1024

            tcp_write(json.dumps(jsonData))
            tcp_flush()

        elif cmd == "halt":
            tcp_write("halting");
            tcp_flush()
            tcp_finish();
            tcp_write(subprocess.check_output(["halt"]));

        elif cmd == "quit":
            tcp_write("quitting");
            tcp_flush()
            tcp_finish();
            break;

        elif cmd == "update":
            tcp_write(subprocess.check_output(["apt-get","update"]))
            tcp_write(subprocess.check_output(["apt-get","upgrade", "-y"]))

        elif cmd == "updatescript":
            dest = sys.argv[0]

            tcp_write("Downloading '" + updateUrl + "'\n")
            filename = wget.download(updateUrl)
            tcp_write("Removing '" + dest + "'\n")
            os.remove(dest)
            tcp_write("Moving '" + filename + "' to '" + dest + "'\n")
            os.rename(filename, dest)

            tcp_write("Exiting.\n")
            tcp_flush()
            tcp_finish()

            args = sys.argv[:]
            args.insert(0, sys.executable)
            os.execv(sys.executable, args)
            break

        elif cmd == "reboot":
            tcp_write("rebooting\n");
            tcp_write(subprocess.check_output(["reboot"]));
            tcp_flush()
            tcp_finish();

        else:
            tcp_write("unknown command: '" + cmd + "'")

        tcp_flush()
        tcp_close()
    except socket_error as e:
        print e.strerror
        try:
            tcp_close()
        except:
            pass
    except Exception as e:
        print e
        tcp_write(e)
        tcp_flush()
        tcp_close()
        pass

    time.sleep(0.5)

tcp_finish()